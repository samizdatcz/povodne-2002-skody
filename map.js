function getColor(val) {
  var col;
  if (val == 0) { col = '#f0f0f0' } else
  if (val <= 77) { col = '#fee5d9' } else
  if (val <= 343) { col = '#fcae91' } else
  if (val <= 3847) { col = '#fb6a4a' } else
  if (val <= 15721) { col = '#de2d26' } else
  if (val > 15721) { col = '#a50f15' } else
    {col = "white"}

  var style = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: "lightgray",
      width: 0.7
    }),
    fill: new ol.style.Fill({
      color: col
    })
  })
  return style;
};

function niceNum(val) { // https://stackoverflow.com/a/2901298
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

function makeTooltip(evt) {
 document.getElementById('tooltip').innerHTML = '<b>' + evt.NAZ_CZNUTS3 + '</b><br>Škody po povodních 2002:<b> ' + niceNum(evt.skody_02_mil) + ' milionů Kč</b> (odhad MMR)'
};


var background = new ol.layer.Tile({
    source: new ol.source.OSM({
    url: 'https://interaktivni.rozhlas.cz/tiles/ton_b1/{z}/{x}/{y}.png',
    attributions: [
      new ol.Attribution({ html: 'obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>, odhady škod <a target="_blank" href="https://www.mmr.cz/cs/Uvodni-strana">MMR</a>'})
    ]
  })
})

var labels = new ol.layer.Tile({
  source: new ol.source.OSM({
    url: 'https://interaktivni.rozhlas.cz/tiles/ton_l1/{z}/{x}/{y}.png'
  })
})

var vector = new ol.layer.Vector({
  source: new ol.source.Vector({
    url: './kraje_skody.json',
    format: new ol.format.TopoJSON({
      layers: ['kraje_skody']
    }),
    overlaps: false
  }),
  style: function(feature) {
    return getColor(feature.get('skody_02_mil'))
  }
});

var map = new ol.Map({
  interactions: ol.interaction.defaults({mouseWheelZoom:false}),
  layers: [background, vector, labels],
  target: 'map',
  view: new ol.View({
    center: ol.proj.transform([15.3350758, 49.7417517], 'EPSG:4326', 'EPSG:3857'),
    zoom: 7,
    minZoom: 6,
    maxZoom: 8
  })
});

map.on('pointermove', function(evt) {
  if (evt.dragging) {
    return;
  }
  var pixel = map.getEventPixel(evt.originalEvent);
  if (map.hasFeatureAtPixel(pixel)){
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      makeTooltip(feature.S);
    });
  } else {
    document.getElementById('tooltip').innerHTML = 'Najetím vyberte kraj.'
  }
});

//mobil
map.on('singleclick', function(evt) {
  var pixel = map.getEventPixel(evt.originalEvent);
  if (map.hasFeatureAtPixel(pixel)){
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      makeTooltip(feature.S);
    });
  } else {
    document.getElementById('tooltip').innerHTML = 'Najetím vyberte kraj.'
  }
});